class CreateFundData < ActiveRecord::Migration[6.0]
  def change
    create_table :fund_data do |t|
      t.bigint :scheme_code
      t.numeric :net_assest_value
      t.numeric :repurchase_price
      t.numeric :sale_price
      t.date :date

      t.timestamps
    end
  end
end
