class CreateSchemeData < ActiveRecord::Migration[6.0]
  def change
    create_table :scheme_data do |t|
      t.bigint :scheme_code
      t.text :scheme_name

      t.timestamps
    end
  end
end
