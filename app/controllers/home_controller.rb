class HomeController < ApplicationController
#Homepage Controller when i user reload the oage for the first time. In index used the if contion because 
#we can have the values from the form in url pamanetrs as we don't have to same them in database. 
#We are getting all the funds type from the databse to select form the user to get the NAV value of the prticular Fund
#used formatjs to append the calulated data
	def index
		@user_data_arr=[]#Array to show mupltple calculated records
		if params[:fund_type].nil?
			@fund=SchemeDatum.all#get all funds from one fundhouse
		else
			@currentdate='2019-11-06'
			#query to get the Past navvalue and the currentdate navvalue we can make this query using SP also and can execute SP here also.
			nav_query = "select net_assest_value,date from fund_data as fd, scheme_data as sd where (fd.date='#{params[:date]}' or fd.date='#{@currentdate}') and scheme_name = '#{params[:fund_type]}' and fd.scheme_code = sd.scheme_code"
			begin
				nav_data = ActiveRecord::Base.connection.execute(nav_query)#navvalue from query
			rescue ActiveRecord::RecordNotFound => error
				#can heandel error
				print error
			end

			begin
				nav_data.each do |data|
					if data["date"]==params[:date]
						@navvalue=data["net_assest_value"]#nav value of past date
						@units=(params[:amount].to_f/@navvalue.to_f).round(3)#units from totalamount/nav
					else
						#as i updated the data in Db on last 6-Nov-2019 so my current date is 6-Nov-2019
						@nav_value_current_date=data["net_assest_value"]#nav value of present date
					end
				end
				#check units are nil or not if units are nil then nav of past is not present in the DB
				if !@units.nil?
					@invested_anount=(@nav_value_current_date.to_f * @units).round(2)
					@user_data_arr.push("invested_anount"=>@invested_anount,"units"=>@units,"navvalue"=>@navvalue,"fundname"=>params[:fund_type],"amount"=>params[:amount],"date"=>params[:date])
				else
					@error="Selected Date Nav Value is not found for #{params[:fund_type]}"	
				end
				#Append the data in index.html file using _calculateddata.html file
				respond_to do |format|
					format.js
				end
			rescue ActiveRecord::RecordNotFound => error
				#can heandel error
				print error
			end
		end
	end
end
